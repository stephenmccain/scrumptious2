from django.forms import ModelForm
from django.forms import ModelForm
from meal_plan.models import MealPlan


class MealPlanform(ModelForm):
    class Meta:
        model = MealPlan
        fields = ["name", "recipes", "date"]


class MealPlanDeleteForm(ModelForm):
    class Meta:
        model = MealPlan
        fields = []